function createNewUser() {
    return newUser = {
        firstName: prompt("Please, enter your first name", "Phillip"),
        lastName: prompt("Please, enter your last name", "Andersen"),
        birthday: prompt("Please, enter your date of birth (dd.mm.yyyy)", "10.04.1992"),
        getLogin: function() {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        },
        getAge: function() {
            return new Date().getFullYear() - +this.birthday.slice(-4);
        },
        getPassword: function() {
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
        },
    }
}

document.querySelector('.btn').addEventListener("click", function() {
    let user = createNewUser();
    console.log(user);
    console.log(`user login: ${user.getLogin()}`);
    console.log(`user age: ${user.getAge()}`);
    console.log(`user password: ${user.getPassword()}`);
});