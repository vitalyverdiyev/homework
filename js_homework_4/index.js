function createNewUser() {
    return newUser = {
        firstName: prompt('Please, enter your first name', ''),
        lastName: prompt('Please, enter your last name', ''),
        getLogin: function() {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        }
    }
}

document.querySelector('.btn').addEventListener("click", function() {
    let user = createNewUser();
    console.log(user);
    console.log(`user login: ${user.getLogin()}`);
});